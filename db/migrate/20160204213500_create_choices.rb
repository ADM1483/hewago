class CreateChoices < ActiveRecord::Migration
  def change
    create_table :choices do |t|
      t.string :name
      t.integer :reservation_id
      t.integer :city_id

      t.timestamps null: false
    end
    add_index :choices, :reservation_id
    add_index :choices, :city_id
  end
end
