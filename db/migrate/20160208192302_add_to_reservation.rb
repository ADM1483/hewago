class AddToReservation < ActiveRecord::Migration
  def change
    add_column :reservations, :date_string, :string
    add_column :reservations, :outbound_start_time_string, :string
    add_column :reservations, :return_start_time_string, :string
    add_column :reservations, :step, :string
  end
end
