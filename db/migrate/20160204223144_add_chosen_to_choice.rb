class AddChosenToChoice < ActiveRecord::Migration
  def change
    add_column :choices, :chosen, :boolean
  end
end
