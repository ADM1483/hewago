class CreateTravelers < ActiveRecord::Migration
  def change
    create_table :travelers do |t|
      t.string :first_name
      t.string :last_name
      t.string :id_type
      t.string :id_number
      t.date :id_expiration
      t.string :nationality
      t.string :email
      t.string :phone
      t.date :birthdate
      t.string :gender
      t.string :address
      t.integer :reservation_id

      t.timestamps null: false
    end
    add_index :travelers, :reservation_id
  end
end
