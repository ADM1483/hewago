class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :number_of_traveler
      t.string :from
      t.string :destination
      t.date :date
      t.integer :destination_removed
      t.integer :outbound_start_time
      t.integer :outbound_end_time
      t.integer :return_start_time
      t.integer :return_end_time
      t.integer :price
      t.string :confirmation_number
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :reservations, :user_id
  end
end
