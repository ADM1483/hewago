/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*            Scroll Sub Nav Bar               */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
    $(window).scroll(function(){
        if ($(window).scrollTop() > 65){
            $('#navbar-booking').addClass("fixed");
        } else{
            $('#navbar-booking').removeClass("fixed");
        }
    });
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*            Scroll Summary Box               */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
    $(window).scroll(function() {
        if ($(window).scrollTop() > 115) {
            $('#summary-box').addClass("fixed");
        } else{
            $('#summary-box').removeClass("fixed");
        }

    });
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*         Menu Burger Responsive              */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
	$('#header-icon').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with-sidebar');
    });
    $('#site-hidden').click(function(e){
        $('body').removeClass('with-sidebar');
    });
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*           Search bar responsive             */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
	$('#search-bar-responsive').click(function(e){
		e.preventDefault();
        $('body').toggleClass('with-search-modal');
	});
	$('#close-search-modal').click(function(e){
        $('body').removeClass('with-search-modal');
    });
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*     Show Departure and Back week-end        */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

$(document).ready(function(){
	$('#booking-form input').on('change', function() {
		var ValueDate = $('input[name=weekend]:checked', '#booking-form').val();
		/* Departure variables */
		var RecomposedDayDepartureDate = ValueDate.substr(0,2);
		var RecomposedMonthDepartureDate = ValueDate.substr(2,2);

		/* Back variables */
		var RecomposedDayBackDate = ValueDate.substr(8,2);
		var RecomposedMonthBackDate = ValueDate.substr(10,2);

		document.getElementById("DeparturePlace").innerHTML = RecomposedDayDepartureDate + '/' + RecomposedMonthDepartureDate;
		document.getElementById("BackPlace").innerHTML = RecomposedDayBackDate + '/' + RecomposedMonthBackDate;
	});
});


/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*     			Calculate Total Price          */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*
function getDestinationPrice(){
	var countChecked = function() {
		var destNb = $("#booking-form input[name='towns[]']:checked").length;

		if(destNb < 11){
			destPrix = 10; //((11 - destNb) * 5);
		} else{
			destPrix = 0;
		}
	};
	countChecked();
	$("input[type=checkbox]").on( "click", countChecked );

	return destPrix;
}


function getDepartureHourPrice(){
	var hourPrix = 0;
	$('#booking-form input').on('change', function() {
		var radioValue = $('input[type=radio][name=departureTime]:checked').attr('value');

		if (radioValue != 'whatever'){
			hourPrix = 20;
		} else{
			hourPrix = 0;
		}
	});
	return hourPrix;
}

$(document).ready(function(){
	var basePrix = 175;
	var prixPers = basePrix + getDepartureHourPrice(); /* + getDestinationPrice() + getBackHourPrice()*/
/*
	document.getElementById("prixPers").innerHTML = prixPers;

});
*/

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/*     			Submit form			           */
/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
/* MIS EN COMMENTAIRE CAR FAIT BUG STRIPE // */
/*
$(document).on('submit', '#email-form', function(e) {
     $.ajax({
        url: $(this).attr('action'),
        type: $(this).attr('method'),
        data: $(this).serialize(),
		success : function(html) {
					 alert('Form is successfully submitted');
				  },
		error   : function(html){
					 alert('Something wrong');
				  }
    });
    e.preventDefault();
});
*/