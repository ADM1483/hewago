json.array!(@reservations) do |reservation|
  json.extract! reservation, :id, :number_of_traveler, :from, :destination, :date, :outbound_start_time, :outbound_end_time, :return_start_time, :return_end_time, :price, :confirmation_number, :user_id
  json.url reservation_url(reservation, format: :json)
end
