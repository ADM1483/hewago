json.array!(@travelers) do |traveler|
  json.extract! traveler, :id, :first_name, :last_name, :id_type, :id_number, :id_expiration, :nationality, :email, :phone, :birthdate, :gender, :address
  json.url traveler_url(traveler, format: :json)
end
