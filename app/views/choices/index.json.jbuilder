json.array!(@choices) do |choice|
  json.extract! choice, :id, :name, :reservation_id, :city_id
  json.url choice_url(choice, format: :json)
end
