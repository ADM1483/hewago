class Reservation < ActiveRecord::Base
  belongs_to :user
  has_many :choices,  dependent: :destroy
  accepts_nested_attributes_for :choices
  has_many :cities, through: :choices
  has_many :travelers, dependent: :destroy
  accepts_nested_attributes_for :travelers


  def to_param
    confirmation_number
  end

end
