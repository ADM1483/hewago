class City < ActiveRecord::Base
  has_many :choices
  has_many :reservations, through: :choices
end
