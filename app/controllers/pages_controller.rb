class PagesController < ApplicationController
  def index
    @reservation = Reservation.new
  end

  def about_us
  end

  def contact
  end

  def faq
  end

  def how_it_works
  end

  def soldout
  end

  def terms
  end

  def testimonials
  end
end
