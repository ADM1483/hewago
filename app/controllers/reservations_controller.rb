class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy, :travelers]
  #before_action :authenticate_user!, only: [:index, :create, :new, :show, :edit, :update, :destroy, :travelers]
  #before_action :correct_user, only: [ :show, :edit, :update, :destroy, :travelers]

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = Reservation.all
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show
  end

  # GET /reservations/new
  def new
  #  @reservation = current_user.reservations.new
    @reservation = Reservation.new
    @city1 = City.find_by_name('Barcelone') || City.create(name: 'Barcelone', intsm: 6)
    @city2 = City.find_by_name('Porto') || City.create(name: 'Porto', intsm: 3)
    @city3 = City.find_by_name('Milan') || City.create(name: 'Milan', intsm: 3)
    @city4 = City.find_by_name('Amsterdam') || City.create(name: 'Amsterdam', intsm: 3)
    @city5 = City.find_by_name('Rome') || City.create(name: 'Rome', intsm: 6)
    @city6 = City.find_by_name('Genève') || City.create(name: 'Genève', intsm: 3)
    @city7 = City.find_by_name('Berlin') || City.create(name: 'Berlin', intsm: 3)
    @city8 = City.find_by_name('Istanbul') || City.create(name: 'Istanbul', intsm: 3)
    @city9 = City.find_by_name('Londres') || City.create(name: 'Londres', intsm: 6)
    @city10 = City.find_by_name('Venise') || City.create(name: 'Venise', intsm: 6)
    @city11 = City.find_by_name('Séville') || City.create(name: 'Séville', intsm: 3)
    @city12 = City.find_by_name('Nice') || City.create(name: 'Nice', intsm: 3)

    @choice1 = @reservation.choices.build
    @choice2 = @reservation.choices.build
    @choice3 = @reservation.choices.build
    @choice4 = @reservation.choices.build
    @choice5 = @reservation.choices.build
    @choice6 = @reservation.choices.build
    @choice7 = @reservation.choices.build
    @choice8 = @reservation.choices.build
    @choice9 = @reservation.choices.build
    @choice10 = @reservation.choices.build
    @choice11 = @reservation.choices.build
    @choice12 = @reservation.choices.build

    @choice1.city_id = @city1.id
    @choice2.city_id = @city2.id
    @choice3.city_id = @city3.id
    @choice4.city_id = @city4.id
    @choice5.city_id = @city5.id
    @choice6.city_id = @city6.id
    @choice7.city_id = @city7.id
    @choice8.city_id = @city8.id
    @choice9.city_id = @city9.id
    @choice10.city_id = @city10.id
    @choice11.city_id = @city11.id
    @choice12.city_id = @city12.id

  end

  # GET /reservations/1/edit
  def edit
  end

  # POST /reservations
  # POST /reservations.json
  def create
    #@reservation = current_user.reservations.build(reservation_params)
    @reservation = Reservation.new(reservation_params)
    @reservation.confirmation_number = SecureRandom.hex(3)
    set_price

    #Create empty travelers
    @reservation.number_of_traveler.times do |i|
      # instance_variable_set("@traveler_#{i+1}", @reservation.travelers.build(first_name: "Traveler #{i+1}"))
      @reservation.travelers.build(first_name: "Traveler #{i+1}")
    end

    respond_to do |format|
      if @reservation.save
        format.html { redirect_to @reservation, notice: 'Reservation was successfully created.' }
        format.json { render :show, status: :created, location: @reservation }
      else
        format.html { render :new }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      format.html { redirect_to reservations_url, notice: 'Reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

def travelers

end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find_by_confirmation_number(params[:id])
    end

    def set_price
      # Set base_price
      base_price = 175

      # set option_destination
      @reservation.destination_removed = 0
      @reservation.choices.each do |c|
        if (c.chosen == false)
          @reservation.destination_removed += 1
        end
       end
      if (@reservation.destination_removed == (0 || 1) )
        option_destination = 0
      else
        option_destination = 5 * (@reservation.destination_removed - 1)
      end

      # Set option_outbound
      if @reservation.outbound_start_time_string == 'whatever'
        option_outbound = 0
      else
        option_outbound = 20
      end
      
      # Set option_return
      if @reservation.return_start_time_string == 'whatever'
        option_return = 0
      else
        option_return = 20
      end

      #  Set price
      @reservation.price = (base_price + option_destination + option_outbound + option_return) * @reservation.number_of_traveler
    end

    def correct_user
      @reservation = current_user.reservations.find_by_confirmation_number(params[:id])
      redirect_to reservations_path, notice: "Vous ne pouvez pas accéder à cette réservation" if @reservation.nil?
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(
          :number_of_traveler,
          :from,
          :destination,
          :date,
          :date_string,
          :destination_removed,
          :outbound_start_time_string,
          :return_start_time_string,
          :outbound_start_time,
          :outbound_end_time,
          :return_start_time,
          :return_end_time,
          :price,
          :confirmation_number,
          :user_id,
          choices_attributes: [:id, :chosen, :city_id],
          travelers_attributes: [:id, :first_name, :last_name, :id_type, :id_number, :id_expiration, :nationality, :email, :phone, :birthdate, :gender, :address])
    end
end
